## [6.6.3] - 2022-10-07
### Added
- Buffer end called on play event

## [6.6.2] - 2022-07-11
### Added
- Avoid buffer being started when the content is paused

## [6.6.1] - 2022-05-17
### Added
- Buffer events

### Updated
- Player version
 
## [6.6.0] - 2021-09-22
### Added
- Error code and description
- Releae version
 
