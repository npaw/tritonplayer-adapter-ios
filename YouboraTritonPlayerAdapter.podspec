Pod::Spec.new do |s|

  # Metadata
  s.name         = 'YouboraTritonPlayerAdapter'
  s.version      = '6.6.3'
  s.summary      = 'Adapter to use YouboraLib on TritonPlayer.'

  s.description  = <<-DESC
                    YouboraTritonPlayerAdapter is an adapter used for TritonPlayer.
                   DESC

  s.homepage     = 'https://documentation.npaw.com/'

  # License
  s.license      = { :type => 'MIT', :file => 'LICENSE.md' }

  # Author Metadata
  s.author             = { 'NPAW' => 'support@nicepeopleatwork.com' }

  # Platform sifics
  s.ios.deployment_target = '11.0'

  # Source Location
  s.source       = { :git => 'https://bitbucket.org/npaw/tritonplayer-adapter-ios.git', :tag => s.version }


  # Source Code
  s.source_files  = 'YouboraTritonPlayerAdapter/**/*.{h,m}'
  s.public_header_files = 'YouboraTritonPlayerAdapter/**/*.h'


  # Project Linking
  s.vendored_frameworks = 'TritonPlayerSDK.xcframework'


  # Project Settings
  s.pod_target_xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => '$(inherited) YOUBORATRITONADAPTER_VERSION=' + s.version.to_s }
  
  # Dependency
  s.dependency 'YouboraLib', '~> 6.5'

end
