//
//  YBTritonPlayerAdsAdapter.m
//  YouboraTritonPlayerAdapter
//
//  Created by Tiago Pereira on 17/11/2020.
//  Copyright © 2020 NPAW. All rights reserved.
//

#import "YBTritonPlayerAdsAdapter.h"
#import "AdapterVersionFactory.h"

#define ADAPTER_VERSION "6.0.0"

@interface YBTritonPlayerAdsAdapter () <TDInterstitialDelegate>

@property (nonatomic) AdapterVersionFactory *versionFactory;
@property (weak) id <TDInterstitialDelegate> oldDelegate;

@end

@implementation YBTritonPlayerAdsAdapter

- (void)registerListeners {
    self.versionFactory = [[AdapterVersionFactory alloc] init];
    [self.versionFactory setPlayerName: @"TritonPlayer"];
    [self.versionFactory setPlayerVersion: TritonSDKVersion];
    [self.versionFactory setAdapterName: @"YBTritonPlayerAdapter"];
    [self.versionFactory setAdapterVersion: @ADAPTER_VERSION];
    [self.versionFactory setAdsAdapter: true];
    
    self.oldDelegate = self.player.delegate;
    self.player.delegate = self.oldDelegate;
}

-(void)unregisterListeners {
    self.player.delegate = self.oldDelegate;
}

#pragma mark Getters

- (NSString *)getPlayerVersion {
    return [self.versionFactory createPlayerVersion];
}

-(NSString *)getPlayerName {
    return [self.versionFactory createPlayerName];
}

- (NSString *)getVersion {
    return [self.versionFactory createAdapterVersion];
}

#pragma mark Delegate functions

- (void)interstitialDidLoadAd:(TDInterstitialAd *)ad {}

- (void)interstitial:(TDInterstitialAd *)ad didFailToLoadAdWithError:(NSError *)error {}

- (void)interstitialWillPresent:(TDInterstitialAd *)ad {}

- (void)interstitialWillDismiss:(TDInterstitialAd *)ad {}

- (void)interstitialDidDismiss:(TDInterstitialAd *)ad {}

- (void)interstitialPlaybackFinished:(TDInterstitialAd *)ad {}

- (void)interstitialWillLeaveApplication:(TDInterstitialAd *)ad {}

@end
