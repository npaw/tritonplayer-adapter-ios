//
//  YBTritonPlayerAdsAdapter.h
//  YouboraTritonPlayerAdapter
//
//  Created by Tiago Pereira on 17/11/2020.
//  Copyright © 2020 NPAW. All rights reserved.
//
#import <YouboraLib/YouboraLib.h>
#import <TritonPlayerSDK/TritonPlayerSDK.h>

@interface YBTritonPlayerAdsAdapter : YBPlayerAdapter<TDInterstitialAd*>

@end

