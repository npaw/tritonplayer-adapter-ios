//
//  AdapterVersionBuilder.m
//  YouboraTritonPlayerAdapter
//
//  Created by Tiago Pereira on 16/11/2020.
//  Copyright © 2020 NPAW. All rights reserved.
//

#import "AdapterVersionFactory.h"

#if TARGET_OS_TV==1
    #define PLUGIN_PLATFORM_DEF "tvOS"
#else
    #define PLUGIN_PLATFORM_DEF "iOS"
#endif

@interface AdapterVersionFactory()

@property (nonatomic) NSString *playerName;
@property (nonatomic) NSString *playerVersion;

@property (nonatomic) NSString *adapterName;
@property (nonatomic) NSString *adapterVersion;
@property (nonatomic) BOOL adsAdapter;

@end

@implementation AdapterVersionFactory

-(NSString*)createPlayerVersion {
    return [NSString stringWithFormat:@"%@-%@",self.playerName, self.playerVersion];
}

-(NSString*)createPlayerName {
    if (self.adsAdapter) {
        return [NSString stringWithFormat:@"%@-Ads-%@",self.playerName, @PLUGIN_PLATFORM_DEF];
    }
    
    return [NSString stringWithFormat:@"%@-%@",self.playerName, @PLUGIN_PLATFORM_DEF];
}

-(NSString*)createAdapterVersion {
    return [NSString stringWithFormat:@"%@-%@",self.adapterVersion, [self createPlayerName]];
}
@end
