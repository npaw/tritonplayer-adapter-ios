//
//  AdapterVersionFactory.h
//  YouboraTritonPlayerAdapter
//
//  Created by Tiago Pereira on 16/11/2020.
//  Copyright © 2020 NPAW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdapterVersionFactory : NSObject

-(void)setPlayerName:(NSString*)playerName;

-(void)setPlayerVersion:(NSString*)playerVersion;

-(void)setAdapterName:(NSString*)adapterName;

-(void)setAdapterVersion:(NSString*)adapterVersion;

-(void)setAdsAdapter:(BOOL)adsAdapter;

-(NSString*)createPlayerVersion;
-(NSString*)createPlayerName;
-(NSString*)createAdapterVersion;
@end
