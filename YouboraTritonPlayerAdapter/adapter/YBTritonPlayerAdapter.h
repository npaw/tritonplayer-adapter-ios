//
//  YBTritonPlayerAdapter.h
//  YouboraTritonPlayerAdapter
//
//  Created by Enrique Alfonso Burillo on 17/09/2019.
//  Copyright © 2019 NPAW. All rights reserved.
//

#import <YouboraLib/YouboraLib.h>
#import <TritonPlayerSDK/TritonPlayerSDK.h>

@interface YBTritonPlayerAdapter : YBPlayerAdapter<TritonPlayer *> <TritonPlayerDelegate>

- (instancetype)initWithPlayer:(TritonPlayer *)player delegate:(id<TritonPlayerDelegate>)delegate settings:(NSDictionary *) settings;

@end
