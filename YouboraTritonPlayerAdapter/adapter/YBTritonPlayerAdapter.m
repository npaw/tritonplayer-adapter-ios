//
//  YBTritonPlayerAdapter.m
//  YouboraTritonPlayerAdapter
//
//  Created by Enrique Alfonso Burillo on 17/09/2019.
//  Copyright © 2019 NPAW. All rights reserved.
//

#import "YBTritonPlayerAdapter.h"
#import <TargetConditionals.h>
#import <YouboraLib/YouboraLib-Swift.h>

// Constants
#define MACRO_NAME(f) #f
#define MACRO_VALUE(f) MACRO_NAME(f)

#define PLUGIN_VERSION_DEF MACRO_VALUE(YOUBORATRITONADAPTER_VERSION)
#define PLUGIN_NAME_DEF "TritonPlayer"

#if TARGET_OS_TV==1
    #define PLUGIN_PLATFORM_DEF "tvOS"
#else
    #define PLUGIN_PLATFORM_DEF "iOS"
#endif

#define PLUGIN_NAME @PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF
#define PLUGIN_VERSION @PLUGIN_VERSION_DEF "-" PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF


@interface YBTritonPlayerAdapter()

@property (nonatomic) NSDictionary *cuePointData;
@property (nonatomic) NSString *cuePointType;
@property (nonatomic, weak) id<TritonPlayerDelegate> delegate;

@end

@implementation YBTritonPlayerAdapter

- (instancetype)initWithPlayer:(TritonPlayer *)player delegate:(id<TritonPlayerDelegate>)delegate settings:(NSDictionary *)settings {
    self.delegate = delegate;
    self = [super initWithPlayer:[player initWithDelegate:self andSettings:settings]];
    return self;
}

- (void)unregisterListeners {
    self.delegate = nil;
}

#pragma mark Getters

- (NSString *)getPlayerVersion {
    return @PLUGIN_NAME_DEF;
}

-(NSString *)getPlayerName {
    return PLUGIN_NAME;
}

- (NSString *)getVersion {
    return PLUGIN_VERSION;
}

- (NSString *)getResource {
    return [self.player getSideBandMetadataUrl];
}

- (NSString *)getProgram {
    if (!self.cuePointData) { return nil; }
    
    if (self.cuePointType && [self.cuePointType isEqualToString:@"ad"]) {
        return [NSString stringWithFormat:@"%@-Ad",self.cuePointData[CommonCueTitleKey]];
    }
    
    return [NSString stringWithFormat:@"%@-%@",self.cuePointData[CommonCueTitleKey],self.cuePointData[TrackArtistNameKey]];
}

#pragma mark Delegate Methods
- (void) player:(TritonPlayer *) player didChangeState:(TDPlayerState) state {
    if (self.delegate && [self.delegate respondsToSelector:@selector(player:didChangeState:)]) {
           [self.delegate player:player didChangeState:state];
    }
    // Commented this condition as cuePointType is not allways set
//    if (!self.cuePointType) {
//        return;
//    }
    switch (state) {
        case kTDPlayerStateCompleted: /// The end of the media has been reached. Not available for live streams.
        case kTDPlayerStateStopped: /// Player is stopped. The playhead position returns to the beginning.
            [self fireStop];
            break;
        case kTDPlayerStatePlaying: /// Player is currently playing
            [self fireBufferEnd];
            [self fireSeekEnd];
            [self fireResume];
            [self fireJoin];
            break;
        case kTDPlayerStateConnecting: /// Player is connecting to the stream
            if (!self.flags.paused) {
                [self fireBufferBegin];
            }
            [self fireStart];
            break;
        case kTDPlayerStatePaused: /// Player is paused. The playhead position is kept at the last position. Not available for live streams.
            [self firePause];
            break;
        case kTDPlayerStateError: /// Player is on error state
            [self fireErrorWithMessage:player.error.localizedDescription code:[@(player.error.code) stringValue] andMetadata:nil];
            break;
    }
}

- (void) player:(TritonPlayer *) player didReceiveInfo:(TDPlayerInfo) info andExtra:(NSDictionary *) extra {
    if (self.delegate && [self.delegate respondsToSelector:@selector(player:didReceiveInfo:andExtra:)]) {
           [self.delegate player:player didReceiveInfo:info andExtra:extra];
    }
    
    /// The stream is buffering. The buffer percentage can be queried by the key InfoBufferingPercentageKey in the extra dictionary for player:didReceiveInfo:andExtra:
    if (info == kTDPlayerInfoBuffering) {
        [YBLog debug:@"player didReceiveInfo: kTDPlayerInfoBuffering andExtra: %@", [extra description]];
    }
    /// The player established connection with the stream. It will soon start playing.
    else if (info == kTDPlayerInfoConnectedToStream) {
        [YBLog debug:@"player didReceiveInfo: kTDPlayerInfoConnectedToStream andExtra: %@", [extra description]];
        [self fireBufferEnd];
    }
    /// The provided mount is geoblocked. The player was redirected to an alternate mount. It's mount name can be obtained by the key InfoAlternateMountNameKey in the extra dictionary.
    else if (info == kTDPlayerInfoForwardedToAlternateMount) {
        [YBLog debug:@"player didReceiveInfo: kTDPlayerInfoForwardedToAlternateMount andExtra: %@", [extra description]];
    }
}

- (void) player:(TritonPlayer *) player didReceiveCuePointEvent:(CuePointEvent *) cuePointEvent {
    if (self.delegate && [self.delegate respondsToSelector:@selector(player:didReceiveCuePointEvent:)]) {
           [self.delegate player:player didReceiveCuePointEvent:cuePointEvent];
    }
    if (!self.cuePointData && !self.cuePointType) {
        self.cuePointData = cuePointEvent.data;
        self.cuePointType = cuePointEvent.type;
        
        [self player:player didChangeState:player.state];
    } else {
        self.cuePointData = cuePointEvent.data;
        self.cuePointType = cuePointEvent.type;
    }
}

- (void) playerBeginInterruption:(TritonPlayer *) player {
    if (self.delegate && [self.delegate respondsToSelector:@selector(playerBeginInterruption:)]) {
           [self.delegate playerBeginInterruption:player];
    }
    if (!self.flags.paused) {
        [self fireBufferBegin];
    }
}

- (void) playerEndInterruption:(TritonPlayer *) player {
    if (self.delegate && [self.delegate respondsToSelector:@selector(playerEndInterruption:)]) {
           [self.delegate playerEndInterruption:player];
    }
    [self fireBufferEnd];
}

- (void) player:(TritonPlayer *) player didReceiveMetaData:(NSDictionary *) metaData {
    if (self.delegate && [self.delegate respondsToSelector:@selector(player:didReceiveMetaData:)]) {
           [self.delegate player:player didReceiveMetaData:metaData];
    }
    [YBLog debug:@"player didReceiveMetaData:\n%@", [metaData description]];
}

@end
