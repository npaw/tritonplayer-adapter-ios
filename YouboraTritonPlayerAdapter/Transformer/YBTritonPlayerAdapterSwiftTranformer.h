//
//  YBTritonPlayerAdapterSwiftTranformer.h
//  YouboraTritonPlayerAdapter
//
//  Created by Elisabet Massó on 29/01/2021.
//  Copyright © 2021 NPAW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YBTritonPlayerAdapter.h"

@interface YBTritonPlayerAdapterSwiftTranformer : NSObject

+(YBPlayerAdapter<id>*)transformFrom:(id)adapter;

@end
