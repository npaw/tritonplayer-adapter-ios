//
//  YBTritonPlayerAdapterSwiftTranformer.m
//  YouboraTritonPlayerAdapter
//
//  Created by Elisabet Massó on 29/01/2021.
//  Copyright © 2021 NPAW. All rights reserved.
//

#import "YBTritonPlayerAdapterSwiftTranformer.h"

@implementation YBTritonPlayerAdapterSwiftTranformer

+(YBPlayerAdapter<id>*)transformFrom:(id)adapter {
    if ([adapter isKindOfClass:[YBPlayerAdapter class]]) {
        return adapter;
    }
    return nil;
}

@end
