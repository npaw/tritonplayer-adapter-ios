//
//  YouboraTritonPlayerAdapter.h
//  YouboraTritonPlayerAdapter
//
//  Created by Enrique Alfonso Burillo on 12/07/2019.
//  Copyright © 2019 NPAW. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for YouboraTritonPlayerAdapter.
FOUNDATION_EXPORT double YouboraTritonPlayerAdapterVersionNumber;

//! Project version string for YouboraTritonPlayerAdapter.
FOUNDATION_EXPORT const unsigned char YouboraTritonPlayerAdapterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YouboraTritonPlayerAdapter/PublicHeader.h>
#import <YouboraTritonPlayerAdapter/YBTritonPlayerAdapter.h>
#import <YouboraTritonPlayerAdapter/YBTritonPlayerAdsAdapter.h>
#import <YouboraTritonPlayerAdapter/YBTritonPlayerAdapterSwiftTranformer.h>

