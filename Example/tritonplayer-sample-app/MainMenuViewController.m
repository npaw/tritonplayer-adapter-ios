//
//  MainMenuViewController.m
//  tritonplayer-sample-app
//
//  Copyright (c) 2015 Triton Digital. All rights reserved.
//

#import "MainMenuViewController.h"
#import <TritonPlayerSDK/TritonPlayerSDK.h>
@import YouboraConfigUtils;
@implementation MainMenuViewController

-(void)viewDidLoad {
    self.clearsSelectionOnViewWillAppear = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    self.navigationItem.title = [NSString stringWithFormat:@"Triton iOS SDK sample - SDK %@", TritonSDKVersion];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Check if Youbora Settings
    NSIndexPath *youboraSettingsIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
    if (indexPath == youboraSettingsIndexPath) {
        UIViewController *configViewController = [YouboraConfigViewController initFromXIBWithAnimatedNavigation:false];
        [self.navigationController pushViewController:configViewController animated:true];
    }
}

@end
